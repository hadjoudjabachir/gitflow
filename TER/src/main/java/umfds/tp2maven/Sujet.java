package umfds.tp2maven;

public class Sujet{
	private String id;
	private String titre;
	
	public Sujet( String id, String titre) {
		this.id = id;
		this.setTitre(titre);
	}
	public Sujet() {}
	
	public void setTitre(String t) {
		this.titre = t;
	}
	
	public String getTitre() {
		return this.titre;
	}
	public String getID() {
		return this.id;
	}
	public String toString() {
		return "id: " + this.id+" ; titre: " + this.titre +"";
	}
}