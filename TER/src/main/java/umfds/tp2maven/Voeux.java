package umfds.tp2maven;

public class Voeux{
	private String idGroupe;
	private String idSujet;
	private int ordre;
	public Voeux(String idG, String idS, int ordre) {
		this.idGroupe = idG;
		this.idSujet = idS;
		this.setOrdre(ordre);
	}
	
	private void setOrdre(int o) {
		if (o <5 && o>=0) {
			this.ordre=o;
		}
		else {
			System.out.println("L'ordre n'est pas correct, veuillez insérer un nombre entre 0 et 5 non compris.\n");
		}
	}
	
	public String getIdG() {
		return this.idGroupe;
	}
	
	public String getIdS() {
		return this.idSujet;
	}
	public int getOrdre() {
		return this.ordre;
	}
}