package umfds.tp2maven;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.exc.StreamWriteException;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.FileWriter;

public class Main{
	
	public static void main(String argmuents[]){
		//Création des fichiers pour l'écriture
		File fg = new File("groupe.json");
		File fs = new File("sujet.json");
		File fv = new File("voeux.json");
		
		//Création des groupes:
		Groupe un = new Groupe("1", "un");
		Groupe deux = new Groupe("2", "deux");
		Groupe trois = new Groupe("3", "trois");
		
		//Creation de la liste des groupes
		ArrayList<Groupe> groupes = new ArrayList<>();
		groupes.add(un);
		groupes.add(deux);
		groupes.add(trois);
		
		//Ecriture au format JSON des groupes
		ObjectMapper grObjMap = new ObjectMapper();
		FileWriter grFileWriter;
		try {
			grFileWriter = new FileWriter("groupe.json");
			try {
				grObjMap.writeValue(grFileWriter,  groupes);
				
			} catch (StreamWriteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (DatabindException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			grFileWriter.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		//Creation des sujets
		Sujet s1 = new Sujet("1", "Sujet 1");
		Sujet s2 = new Sujet("2", "Sujet 2");
		Sujet s3 = new Sujet("3", "Sujet 3");
		Sujet s4 = new Sujet("4", "Sujet 4");
		Sujet s5 = new Sujet("5", "Sujet 5");
		Sujet s6 = new Sujet("6", "Sujet 6");
		
		//Creation de la liste des sujets:
		ArrayList<Sujet> sujets = new ArrayList<>();
		sujets.add(s1); sujets.add(s2);  sujets.add(s3);
		sujets.add(s4); sujets.add(s5); sujets.add(s6);
		
		//Ecriture au format JSON des sujets:
		ObjectMapper sujObjMap = new ObjectMapper();
		FileWriter sujFileWriter;
		try {
			sujFileWriter = new FileWriter("sujet.json");
			try {
				sujObjMap.writeValue(sujFileWriter,  sujets);
				
			} catch (StreamWriteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (DatabindException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			sujFileWriter.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		//Creation des voeux
 		Voeux G1_1 = new Voeux(un.getId(), s1.getID(), 0);
		Voeux G1_2 = new Voeux(un.getId(), s2.getID(), 1);
		Voeux G1_3 = new Voeux(un.getId(), s3.getID(), 2);
		Voeux G1_4 = new Voeux(un.getId(), s4.getID(), 3);
		Voeux G1_5 = new Voeux(un.getId(), s5.getID(), 4);
		
		Voeux G2_1 = new Voeux(deux.getId(), s1.getID(), 3);
		Voeux G2_2 = new Voeux(deux.getId(), s2.getID(), 2);
		Voeux G2_3 = new Voeux(deux.getId(), s3.getID(), 1);
		Voeux G2_4 = new Voeux(deux.getId(), s4.getID(), 4);
		Voeux G2_5 = new Voeux(deux.getId(), s5.getID(), 0);
		
		Voeux G3_1 = new Voeux(trois.getId(), s1.getID(), 1);
		Voeux G3_2 = new Voeux(trois.getId(), s2.getID(), 4);
		Voeux G3_3 = new Voeux(trois.getId(), s3.getID(), 2);
		Voeux G3_4 = new Voeux(trois.getId(), s4.getID(), 3);
		Voeux G3_5 = new Voeux(trois.getId(), s5.getID(), 0);
		
		//Creation de la lsite des voeux:
		ArrayList<Voeux> LiVoeux = new ArrayList<>();
		LiVoeux.add(G1_1); LiVoeux.add(G1_2); LiVoeux.add(G1_3); LiVoeux.add(G1_4); LiVoeux.add(G1_5);
		LiVoeux.add(G2_1); LiVoeux.add(G2_2); LiVoeux.add(G2_3); LiVoeux.add(G2_4); LiVoeux.add(G2_5);
		LiVoeux.add(G3_1); LiVoeux.add(G3_2); LiVoeux.add(G3_3); LiVoeux.add(G3_4); LiVoeux.add(G3_5);
		
		//Ecriture au format JSON des voeux:
		ObjectMapper voObjMap = new ObjectMapper();
		FileWriter voFileWriter;
		try {
			voFileWriter = new FileWriter("voeux.json");
			try {
				voObjMap.writeValue(voFileWriter,  LiVoeux);
				
			} catch (StreamWriteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (DatabindException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			voFileWriter.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		//Passage de JSon vers Objet:
		//Pour les groupes:
			Scanner grFileReader;
			try {
				grFileReader = new Scanner(fg);
				String grJson = grFileReader.next();
				
				Groupe[] liGr;
				try {
					liGr = grObjMap.readValue(grJson, Groupe[].class);
					System.out.println(grJson);
					for(int i =0; i<liGr.length; i++) {
						System.out.println(liGr[i].toString());
					}
					grFileReader.close();
				} catch (JsonMappingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JsonProcessingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		
	}
	
}