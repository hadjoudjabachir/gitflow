package umfds.tp2maven;

public class Groupe {
	private String id;
	private String nom;
	private Sujet sujet;
	
	public Groupe(String id, String nom) {
		this.id = id;
		this.setNom(nom);
	}
	public Groupe() {
		
	}
	
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public String getNom() {
		return this.nom;
	}
	
	public String getId(){
		return this.id;
	}
	
	public void setSujet(Sujet s) {
		this.sujet = s;
	}
	
	public String toString() {
		return "id: " + this.id + "; nom: " + this.nom + (this.sujet==null? "" : "sujet: " +this.sujet);
	}
	
}